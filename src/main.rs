extern crate rand;

use std::io;
use std::cmp::Ordering;
use rand::Rng;

fn main() {
    println!("I heavily doubt you'll geuss my number... I'll give you a clue though and tell that it is between 1 and 1000");
    
    let secret_number = rand::thread_rng().gen_range(1, 1001);

loop {

    println!("go ahead and try, I'll be waiting for you to figure out the obvious (If you don't know the guess should be a number you have already lost) :)");

    let mut guess = String::new();

    io::stdin().read_line(&mut guess)
        .expect("Failed to read line");

    let guess: u32 = match guess.trim().parse() {
    Ok(num) => num,
    Err(_) => continue,
    };

    match guess.cmp(&secret_number) {
        Ordering::Less => println!("Oh please, are you telling me that you honestly think {} is the correct number? You have to go bigger, you should know that...", guess),
        Ordering::Greater => println!("Oh please, are you telling me that you honestly think {} is the correct number? You have to go smaller, you should know that", guess),
        Ordering::Equal => {
            println!("Took you long enough...");
            break;
            }
        }
    }   
}
